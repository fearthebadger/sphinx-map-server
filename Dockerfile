FROM sphinx-server:latest

MAINTAINER Brock Renninger <brock@fearthebadger.com>

COPY ./requirements.txt requirements.txt

ENV PYTHONPATH /usr/lib/python3.8/site-packages

RUN apk add --no-cache --virtual --update grep \
    && pip install --upgrade pip \
    && pip install --no-cache-dir  -r requirements.txt

RUN wget https://unpkg.com/vis-network@8.2.1/standalone/umd/vis-network.min.js -P /opt/sphinx-server/

COPY ./scripts/notes_map.html /opt/sphinx-server/
COPY ./scripts/make_map.py /usr/lib/python3.8/site-packages/

WORKDIR /web

EXPOSE 8000

CMD ["python", "/opt/sphinx-server/server.py"]
