#!/usr/bin/env python3

import os
from os import path
from shutil import copyfile
import subprocess
import re

def find_children(folder, note_links, nodes):

    for f in os.scandir(folder):

        if f.is_dir() and f.name not in [".git", "_build"]:

            child_folder = folder + f.name + "/"

            if path.exists(child_folder + "/index.rst"):

                nodes.append(child_folder)

                note_link = {}
                note_link['link_start'] = folder
                note_link['link_end'] = child_folder
                note_link['add_ons'] = ""
                note_links.append(note_link)
                note_links, nodes = find_children( child_folder, note_links, nodes )

    return note_links, nodes

def find_doc_links(note_links):

    add_ons = ", smooth: { type: 'discrete' }, hoverWidth: 0, dashes: true, color: { color: '#DDDDDD' }"

    stdout = subprocess.check_output(
            "/bin/grep -Ro --include=*.rst -e \\:ref\\:\\`[^\\\"]*\\` ./",
        universal_newlines=True,
        shell=True
    )

    link_start = stdout.splitlines()

    for link in link_start:

        link_part = link.split('::ref:')

        link_start_dir = path.dirname(link_part[0]) + "/"

        link_word = link_part[1].split('<')[1]
        link_word = link_word.split('>')[0]


        # The exit 0 is a stupid work around to grep returning a 1 if nothing is
        # found.
        stdout = subprocess.check_output(
            "/bin/grep -Rl --include=*.rst -e ..\\\\s_" + link_word + ":$ ./; exit 0",
            universal_newlines=True,
            shell=True
        )

        if stdout:
            # You should never have more than one link originator. So I'm going
            # to ignore anything but the first one.
            link_end = stdout.splitlines()[0]
            link_end_dir = path.dirname(link_end) + "/"

            if link_start_dir != link_end_dir:

                note_link = {}
                note_link['link_start'] = link_start_dir
                note_link['link_end'] = link_end_dir
                note_link['add_ons'] = add_ons
                note_links.append(note_link)

    return note_links


def write_js_script(note_links, nodes, build_folder):

    fp = open(build_folder + "/notes_map.js", "w")

    fp.write("var nodes = new vis.DataSet([\n")
    option_groups = ""

    for index, node in enumerate(nodes):

        group = ""
        label = ""

        # Using readlines()
        index_fh = open('./' + node + 'index.rst', 'r')
        lines = index_fh.readlines()

        # Strips the newline character
        for line in lines:

            if re.match('^.. desc:\\s', line):
                label = re.sub('^.. desc:\\s', '', line).rstrip()
            elif re.match("^.. option_group:\\s", line):
                option_groups += re.sub('^.. option_group:\\s', '', line).rstrip() + ',\n'
            elif re.match("^.. group:\\s", line):
                group = re.sub('^.. group:\\s', '', line).rstrip()

        index_fh.close()

        fp.write("  {id: " + str(index))
        fp.write(", label: '" + label + "'")
        fp.write(", url: '" + node + "index.html'")
        fp.write(", group: '" + group + "'")
        fp.write(", shape: 'box'")
        fp.write("},\n")

    fp.write("]);")

    fp.write("\n\n")

    fp.write("var edges = new vis.DataSet([\n")

    for note_link in note_links:

        fp.write("  {from: " + str(nodes.index(note_link['link_start'])))
        fp.write(", to: " + str(nodes.index(note_link['link_end'])))
        fp.write(note_link['add_ons'] + "},\n")

    fp.write("]);")

    fp.write("\n\n")

    fp.write("var container = document.getElementById('mynotes');\n")

    fp.write("var data = {\n")
    fp.write("  nodes: nodes,\n")
    fp.write("  edges: edges,\n")
    fp.write("};")

    fp.write("\n\n")

    fp.write("var options = {\n")
    fp.write("  groups:{\n")
    fp.write("    " + option_groups)
    fp.write("  },\n")
    fp.write("  interaction: { hover: true },\n")
    fp.write("  nodes:{\n")
    fp.write("    borderWidth: 1,\n")
    fp.write("    chosen: true,\n")
    fp.write("    color: {\n")
    fp.write("      border: '#AAAAAA',\n")
    fp.write("      background: '#EEEEEE',\n")
    fp.write("      highlight: {\n")
    fp.write("        border: '#2B7CE9',\n")
    fp.write("        background: '#D2E5FF'\n")
    fp.write("      },\n")
    fp.write("      hover: {\n")
    fp.write("        border: '#2B7CE9',\n")
    fp.write("        background: '#D2E5FF'\n")
    fp.write("      }\n")
    fp.write("    }\n")
    fp.write("  }\n")
    fp.write("}\n")

# □ Build groups based on the first children indexes of Notes.
# □ Build dotted lines when linking between groups.
# □ Anchor the notes index to the center. (Maybe... the dashes may distinguish
#   enough
# □ Make the nodes a link to the notes page.
# ✓ Change the shape of the bubbles to rounded rectangles.
#   ✓ With light gray background and colored border.
#
#    var options = {
#      groups: {
#        test: {
#          color:{
#            background:'red',
#            border: 'green'
#          },
#          borderWidth: 10
#        }
#      }
#    }

    fp.write("\n")
    fp.write("var network = new vis.Network(container, data, options);\n")

    fp.write("\n")
    fp.write("network.on(\"doubleClick\", function(properties) {\n")
    fp.write("  if(!properties.nodes.length) return;\n")
    fp.write("  var node = nodes.get(properties.nodes[0]);\n")
    fp.write("  window.open(node.url, \"_top\");\n")
    fp.write("});\n")

    fp.close()

def make_map (build_folder):
    note_links = []
    nodes = ["./"]

    if not path.exists(build_folder + '/vis-network.min.js'):
        copyfile("/opt/sphinx-server/vis-network.min.js", build_folder + "/vis-network.min.js")

    if not path.exists(build_folder + '/notes_map.html'):
        copyfile("/opt/sphinx-server/notes_map.html", build_folder + "/notes_map.html")

    note_linksfolder_dict, nodes = find_children("./", note_links, nodes)
    note_linksfolder_dict = find_doc_links(note_linksfolder_dict)

    write_js_script(note_linksfolder_dict, nodes, build_folder)

